name := "Homework1"

scalaVersion := "2.13.8"

resolvers ++= Seq(
	"scala-course-tom"  at "https://gitlab.com/api/v4/projects/33751126/packages/maven",
	"scala-course-john" at "https://gitlab.com/api/v4/projects/33751126/packages/maven"
)

dependencyOverrides ++= Seq(
	"ru.tinkoff" %% "scala-course-clerk" % "0.2"
)

libraryDependencies ++= Seq(
	"ru.tinkoff" %% "scala-course-tom"  % "0.2",
	"ru.tinkoff" %% "scala-course-john" % "0.2"
)